<?php

namespace Drupal\days_data\Plugin\Block;

use Drupal\Core\Annotation\Translation;
use Drupal\Core\Block\Annotation\Block;
use Drupal\Core\Block\BlockBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Drupal\days_data\DaysDataInformer;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Days data Informer block.
 *
 * @Block(
 *   id = "days_data_informer_block",
 *   admin_label = @Translation("Days data informer"),
 * )
 */
class DaysDataInformerBlock extends BlockBase implements ContainerFactoryPluginInterface {

  /**
   * @var \Drupal\days_data\DaysDataInformer
   */
  protected $informer;

  /**
   * {@inheritdoc}
   */
  public function __construct(array $configuration, $plugin_id, $plugin_definition, DaysDataInformer $days_data_informer) {
    parent::__construct($configuration, $plugin_id, $plugin_definition);
    $this->informer = $days_data_informer;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->get('days_data.informer')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function build() {
    $block['content'] = $this->informer->getInformerComponent($this->configuration['message'], $this->configuration['informer_text']);

    return $block;
  }

  /**
   * {@inheritdoc}
   */
  public function blockForm($form, FormStateInterface $form_state) {
    $form = parent::blockForm($form, $form_state);

    $config = $this->getConfiguration();

    $form['message'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Message'),
      '#description' => $this->t('Type the label you want visitors to see'),
      '#default_value' => $config['message'] ?? '',
    ];

    $form['informer_text'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Informer text'),
      '#description' => $this->t('Type the informer text you want visitors to see'),
      '#default_value' => $config['informer_text'] ?? '',
    ];

    return$form;
  }

  /**
   * {@inheritdoc}
   */
  public function blockSubmit($form, FormStateInterface $form_state) {
    parent::blockSubmit($form, $form_state);

    $this->configuration['message'] = $form_state->getValue('message');
    $this->configuration['informer_text'] = $form_state->getValue('informer_text');
  }

}

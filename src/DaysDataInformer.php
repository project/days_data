<?php

namespace Drupal\days_data;

use Drupal\Core\Datetime\DrupalDateTime;
use Drupal\Core\StringTranslation\StringTranslationTrait;

class DaysDataInformer {

  use StringTranslationTrait;

  /**
   * Start date of aggression against Ukraine.
   */
  const WAR_START_DATE = '2022-02-24 03:40:00';

  /**
   * @return array
   */
  public function getInformerComponent(string $label = '', string $informer_text = '') {
    $start_time = new DrupalDateTime(self::WAR_START_DATE);
    $now_time = new DrupalDateTime();
    $days = $now_time->diff($start_time)->days + 1;
    if (!$label) {
      $label = 'Military aggression of rashists in Ukraine:';
    }
    if (!$informer_text) {
      $informer_text = 'day of the war';
    }

    $markup = $this->t('@data', ['@data' => $days]);
    $markup_text = $this->t($informer_text);

    return [
      '#theme' => 'days_data_informer',
      '#label' => $this->t($label),
      '#informer' => [
        '#markup' => $markup,
      ],
      '#informer_text' => [
        '#markup' => $markup_text,
      ],
      '#attached' => [
        'library' => [
          'countup/core',
        ],
      ],
      '#cache' => [
        'contexts' => [],
        'tags' => [],
        'max-age' => 0,
      ],
    ];
  }

}
